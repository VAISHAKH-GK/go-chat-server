package server

import (
	"github.com/gofiber/fiber/v2"
)

// Start the server
func Run(port string) error {
	var app = fiber.New()

	app.Get("/test", func(c *fiber.Ctx) error {
		return c.SendString("Succes")
	})

	return app.Listen(":" + port)
}
