package main

import (
	"os"

	"github.com/VAISHAKH-GK/go-chat-server/server"
	"github.com/joho/godotenv"
)

func main() {
	// Getting port from enviornment variables
	var port string
	godotenv.Load(".env")
	if port = os.Getenv("PORT"); port != "" {
		port = "8080"
	}

	// Starting http server
	server.Run(port)
}
